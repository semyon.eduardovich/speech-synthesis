# SOVA TTS

SOVA TTS is a speech syntthesis solution based on [Tacotron 2](https://arxiv.org/abs/1712.05884) architecture. It is designed as a REST API service and it can be customized (both code and models) for your needs.

## Team
      
     Mironova Anastasia
     Andreev Stanislav
     Khafizov Eduard
     Vasilev Semyon
     


## Installation

*   Clone the repository, download the pretrained models archive and extract the contents into the project folder:
```bash
 git clone --recursive https://gitlab.com/semyon.eduardovich/speech-synthesis.git --branch v1.1
 cd sova-tts/
 wget http://dataset.sova.ai/SOVA-TTS/Data_v1.1.tar
 tar -xvf Data_v1.1.tar && rm Data_v1.1.tar
```

*   Build docker image
     ```
      sudo docker-compose build sova-tts
     ```

*	Run the desired service container
     ```
     docker-compose up -d sova-tts
     ```
