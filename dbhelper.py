import sqlite3
from sqlite3 import IntegrityError

import os

class dbhelper:
    def open_connection(self):
        self.con = sqlite3.connect('sqlite.db')
        self.cur = self.con.cursor()
        create_table = """CREATE TABLE IF NOT EXISTS waves (
                            filename text PRIMARY KEY);"""
        self.cur.execute(create_table)
        self.con.commit()

    def get_history(self):
        self.open_connection()
        select = "SELECT * FROM waves"
        self.cur.execute(select)
        query_result = self.cur.fetchall()
        result = []
        for each in query_result:
            result.append(each[0])
        self.con.close()
        return result

    def add_to_db(self, filename):
        self.open_connection()
        insert = f"INSERT INTO waves values ('{filename}');"
        try:        
            self.cur.execute(insert)
            self.con.commit()
            self.con.close()
            result = f"Файл {filename} успешно добавлен"
        except IntegrityError:
            print ("Такое значение уже существует")
            self.con.close()
            return None
        return result

    def del_from_db(self, filename):
        
        self.open_connection()
        
        try:
            delete = f"DELETE FROM waves WHERE filename = '{filename}'"
            self.cur.execute(delete)
            self.con.commit()
            self.con.close()

            os.remove(filename)

            result = f"Файл {filename} успешно удален"
            return result

        except:
            print ("Такого значения не существует")
            self.con.close() # можно добавить
            return None
            
        return result