from fastapi import FastAPI, Request, Form, HTTPException, Depends
from fastapi.responses import RedirectResponse, FileResponse, StreamingResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles


import uvicorn
from dbhelper import dbhelper

from file_handler import FileHandler
from models import models, ALL_MODELS
from base64 import b64encode

templates = Jinja2Templates(directory="templates")

app = FastAPI()
app.debug = True
app.mount('/static', StaticFiles(directory="static"),name="static")

_valid_model_types = [key for key in models if key is not ALL_MODELS]

@app.get("/")
def return_index(request: Request):
    return templates.TemplateResponse("speechSynthesis.html", {"request": request, "existing_models":models.keys()})

@app.post("/synthesize/")
async def synthesize(request: Request):
    request_json = await request.json()

    text = request_json["text"]
    model_type = request_json["voice"]

    options = {
        "rate": float(request_json.get("rate", 1.0)),
        "pitch": float(request_json.get("pitch", 1.0)),
        "volume": float(request_json.get("volume", 0.0))
    }

    response_code, results = FileHandler.get_synthesized_audio(text, model_type, **options)

    if response_code == 0:
        for result in results:
            filename = result.pop("filename")
            print (filename)
            dbhelper().add_to_db(filename)
            audio_bytes = result.pop("response_audio")
            result["response_audio_url"] = request.url_for("media_file", filename = filename)
            
            result["response_audio"] = b64encode(audio_bytes).decode("utf-8")

    return {
        "response_code": response_code,
        "response": results
    }

class InvalidVoice(Exception):
    pass

@app.post("/get_user_dict/")
async def get_user_dict(request: Request):
    request_json = await request.json()

    model_type = request_json.get("voice")

    response_code = 1
    try:
        if model_type not in _valid_model_types:
            raise InvalidVoice("Parameter 'voice' must be one of the following: {}".format(_valid_model_types))

        model = models[model_type]
        result = model.get_user_dict()

        response_code = 0
    except InvalidVoice as e:
        result = str(e)
    except Exception as e:
        result = str(e)

    return {
        "response_code": response_code,
        "response": result
    }

@app.post("/update_user_dict/")
async def update_user_dict(request: Request):
    request_json = await request.json()

    model_type = request_json.get("voice")
    user_dict = request_json.get("user_dict")

    response_code = 1
    try:
        if model_type not in _valid_model_types:
            raise InvalidVoice("Parameter 'voice' must be one of the following: {}".format(_valid_model_types))

        model = models[model_type]
        model.update_user_dict(user_dict)

        result = "User dictionary has been updated"
        response_code = 0
    except InvalidVoice as e:
        result = str(e)
    except Exception as e:
        result = str(e)

    return {
        "response_code": response_code,
        "response": result
    }

@app.post("/replace_user_dict/")
async def replace_user_dict(request: Request):
    request_json = await request.json()

    model_type = request_json.get("voice")
    user_dict = request_json.get("user_dict")

    response_code = 1
    try:
        if model_type not in _valid_model_types:
            raise InvalidVoice("Parameter 'voice' must be one of the following: {}".format(_valid_model_types))

        model = models[model_type]
        model.replace_user_dict(user_dict)

        result = "User dictionary has been replaced"
        response_code = 0
    except InvalidVoice as e:
        result = str(e)
    except Exception as e:
        result = str(e)

    return {
        "response_code": response_code,
        "response": result
    }

@app.get("/media/{filename:path}")
async def media_file(filename:str):
    audio_file = open(filename, mode="rb")
    return StreamingResponse(audio_file, media_type="audio/wav")
    #return filename
    #return FileResponse (".", filename) 
    #return send_from_directory(".", filename, as_attachment=False)

if __name__ == "__main__":
    uvicorn.run(
        app=app,
        host="0.0.0.0",
        port=8899
    )